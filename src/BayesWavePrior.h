
/*
 *  Copyright (C) 2018 Neil J. Cornish, Tyson B. Littenberg, Margaret Millhouse
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with with program; see the file COPYING. If not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 *  MA  02111-1307  USA
 */

void set_bayesline_priors(char *channel, struct BayesLineParams *bayesline, double Tobs, int SnModel);

int checkrange(double *p, double **r, int NW);

int extrinsic_checkrange(double *p);

void proximity_normalization(double **params, double **range, double *norm, int *larray, int cnt);
double wavelet_proximity_density(double f, double t, double **params, int *larray, int cnt, struct Prior *prior);
double wavelet_proximity_prior(struct Wavelet *wave, struct Prior *prior);

double glitch_background_prior(struct Prior *prior, double *params);

double signal_amplitude_prior (double *params, double *Snf, double Tobs, double SNRpeak);
double glitch_amplitude_prior (double *params, double *Snf, double Tobs, double SNRpeak);
double uniform_amplitude_prior(double *params, double *Snf, double Sa, double Tobs, double **range);


