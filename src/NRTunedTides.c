/*
 * Copyright (C) 2017-2021 Tim Dietrich, Sebastiano Bernuzzi, Nathan Johnson-McDaniel,
 * Shasvath J Kapadia, Francesco Pannarale and Sebastian Khan, Michael Puerrer,
 * Katerina Chatziioannou, Neil Cornish, Sophie Hourihane, Marcella Wijngaarden 
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with with program; see the file COPYING. If not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 *  MA  02111-1307  USA
 */

// LAL independent code 

// TODO: Remove the LAL independent waveform option from BayesCBC (or do double maintanance..)

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <complex.h>
#include "IMRPhenomD_internals.h"

#ifdef __GNUC__
#define UNUSED __attribute__ ((unused))
#else
#define UNUSED
#endif

// Plack taper window
static double PlanckTaper(const double t, const double t1, const double t2) {
  double taper;
  if (t <= t1)
    taper = 0.;
  else if (t >= t2)
    taper = 1.;
  else
    taper = 1. / (exp((t2 - t1)/(t - t1) + (t2 - t1)/(t - t2)) + 1.);
  return taper;
}

/**
 * function to swap masses and lambda to enforece m1 >= m2
 */
static int EnforcePrimaryMassIsm1(double *m1, double *m2, double *lambda1, double *lambda2){
  char errstr[200];

  // if ((*m1 == *m2) && (*lambda1 != *lambda2)) {
  //   snprintf(errstr, strlen(errstr),"m1 == m2 (%g), but lambda1 != lambda2 (%g, %g).\n", *m1, *lambda1, *lambda2);
  //   PRINT_WARNING(errstr);
  // }

  double lambda1_tmp, lambda2_tmp, m1_tmp, m2_tmp;
  
  if (*m1>=*m2) {
    lambda1_tmp = *lambda1;
    lambda2_tmp = *lambda2;
    m1_tmp   = *m1;
    m2_tmp   = *m2;
  } else { /* swap spins and masses */
    // printf("!! Swapping lambda1 & 2 internally !! \n");
    lambda1_tmp = *lambda2;
    lambda2_tmp = *lambda1;
    m1_tmp   = *m2;
    m2_tmp   = *m1;
  }
  *m1 = m1_tmp;
  *m2 = m2_tmp;
  *lambda1 = lambda1_tmp;
  *lambda2 = lambda2_tmp;

  if (*m1 < *m2) {
    printf("XLAL_ERROR in EnforcePrimaryMassIsm1.");
    snprintf(errstr, strlen(errstr), "XLAL_ERROR in EnforcePrimaryMassIsm1. When trying\
 to enfore that m1 should be the larger mass.\
 After trying to enforce this m1 = %f and m2 = %f\n", *m1, *m2);
    ERROR(PD_EFUNC, errstr);
  }

  return PD_SUCCESS;
}

/**
 * Internal function only
 * Function to call the frequency domain tidal correction
 * Equation (7) in arXiv:1706.02969
 */
static double SimNRTunedTidesFDTidalPhase(
            const double fHz, /**< Gravitational wave frequency (Hz) */
            const double Xa, /**< Mass of companion 1 divided by total mass */
            const double Xb, /**< Mass of companion 2 divided by total mass */
            const double mtot, /**< total mass (Msun) */
            const double kappa2T /**< tidal coupling constant. Eq. 2 in arXiv:1706.02969 */
            )
{
  /* NRTunedTidesFDTidalPhase is Eq 7 in arXiv:1706.02969
   * and is a function of x = angular_orb_freq^(2./3.)
   */
  double M_omega = PI * fHz * (mtot * MTSUN_SI); //dimensionless angular GW frequency

  double PN_x = pow(M_omega, 2.0/3.0);
  double PN_x_2 = PN_x * PN_x;
  double PN_x_3over2 = pow(PN_x, 3.0/2.0);
  double PN_x_5over2 = pow(PN_x, 5.0/2.0);

  /* model parameters */
  const double c_Newt = 2.4375; /* 39.0 / 16.0 */

  const double n_1 = -17.428;
  const double n_3over2 = 31.867;
  const double n_2 = -26.414;
  const double n_5over2 = 62.362;

  const double d_1 = n_1 - 2.496; /* 3115.0/1248.0 */
  const double d_3over2 = 36.089;

  double tidal_phase = - kappa2T * c_Newt / (Xa * Xb) * PN_x_5over2;

  double num = 1.0 + (n_1 * PN_x) + (n_3over2 * PN_x_3over2) + (n_2 * PN_x_2) + (n_5over2 * PN_x_5over2) ;
  double den = 1.0 + (d_1 * PN_x) + (d_3over2 * PN_x_3over2) ;

  double ratio = num / den;

  tidal_phase *= ratio;

  return tidal_phase;
}

/** 
 * NRTunedTidesFDTidalPhase is Eq 22 of https://arxiv.org/abs/1905.06011 
 * and is a function of x = angular_orb_freq^(2./3.)
 */
static double SimNRTunedTidesFDTidalPhase_v2(
               const double fHz, /**< Gravitational wave frequency (Hz) */
               const double Xa, /**< Mass of companion 1 divided by total mass */
               const double Xb, /**< Mass of companion 2 divided by total mass */
               const double mtot, /**< total mass (Msun) */
               const double kappa2T /**< tidal coupling constant. Eq. 2 in arXiv:1706.02969 */
               )
{

  double M_omega = PI * fHz * (mtot * MTSUN_SI); //dimensionless angular GW frequency
  double PN_x = pow(M_omega, 2.0/3.0);
  double PN_x_2 = PN_x * PN_x;
  double PN_x_3 = PN_x * PN_x_2;
  double PN_x_3over2 = pow(PN_x, 3.0/2.0);
  double PN_x_5over2 = pow(PN_x, 5.0/2.0);
  /* model parameters */
  const double c_Newt   = 2.4375;
  const double n_1      = -12.615214237993088;
  const double n_3over2 =  19.0537346970349;
  const double n_2      = -21.166863146081035;
  const double n_5over2 =  90.55082156324926;
  const double n_3      = -60.25357801943598;
  const double d_1      = -15.111207827736678;
  const double d_3over2 =  22.195327350624694;
  const double d_2      =   8.064109635305156;
  double tidal_phase = - kappa2T * c_Newt / (Xa * Xb) * PN_x_5over2;
  double num = 1.0 + (n_1 * PN_x) + (n_3over2 * PN_x_3over2) + (n_2 * PN_x_2) + (n_5over2 * PN_x_5over2) + (n_3 * PN_x_3);
  double den = 1.0 + (d_1 * PN_x) + (d_3over2 * PN_x_3over2) + (d_2 * PN_x_2) ;
  double ratio = num / den;
  tidal_phase *= ratio;
  return tidal_phase;
}


/** 
 * Tidal amplitude corrections; only available for NRTidalv2;
 * Eq. 24 of arxiv: 1905.06011
 */
static double SimNRTunedTidesFDTidalAmplitude(
					     const double fHz, /**< Gravitational wave frequency (Hz) */
					     const double mtot, /**< Total mass in solar masses */
					     const double kappa2T /**< tidal coupling constant. Eq. 2 in arXiv:1706.02969 */
					     )
{
  const double M_sec   = (mtot * MTSUN_SI);

  double prefac = 0.0;
  prefac = 9.0*kappa2T;

  double x = pow(PI*M_sec*fHz, 2.0/3.0);
  double ampT = 0.0;
  double poly = 1.0;
  const double n1   = 4.157407407407407;
  const double n289 = 2519.111111111111;
  const double d    = 13477.8073677; 

  poly = (1.0 + n1*x + n289*pow(x, 2.89))/(1+d*pow(x,4.));
  ampT = - prefac*pow(x,3.25)*poly;

  return ampT;
}

/**
 * convienient function to compute tidal coupling constant. Eq. 2 in arXiv:1706.02969
 * given masses and lambda numbers
 */
double NRTunedTidesComputeKappa2T(
           double m1_SI, /**< Mass of companion 1 (kg) */
           double m2_SI, /**< Mass of companion 2 (kg) */
           double lambda1, /**< (tidal deformability of mass 1) / m1^5 (dimensionless) */
           double lambda2 /**< (tidal deformability of mass 2) / m2^5 (dimensionless) */
           )
{
  int errcode = EnforcePrimaryMassIsm1(&m1_SI, &m2_SI, &lambda1, &lambda2);
  CHECK(PD_SUCCESS == errcode, errcode, "EnforcePrimaryMassIsm1 failed");

  const double m1 = m1_SI / MSUN_SI;
  const double m2 = m2_SI / MSUN_SI;
  const double mtot = m1 + m2;

  /* Xa and Xb are the masses normalised for a total mass = 1 */
  /* not the masses appear symmetrically so we don't need to switch them. */
  const double Xa = m1 / mtot;
  const double Xb = m2 / mtot;

  /**< tidal coupling constant. This is the
    kappa^T_eff = 2/13 [  (1 + 12 X_B/X_A) (X_A/C_A)^5 k^A_2 +  [A <- -> B]  ]
    from Tim Dietrich */

  /* Note that 2*k_2^A/c_A^5 = 3*lambda1 */
  const double term1 = ( 1.0 + 12.0*Xb/Xa ) * pow(Xa, 5.0) * lambda1;
  const double term2 = ( 1.0 + 12.0*Xa/Xb ) * pow(Xb, 5.0) * lambda2;
  const double kappa2T = (3.0/13.0) * ( term1 + term2 );

  return kappa2T;
}

/**
 * compute the merger frequency of a BNS system.
 * Tim's new fit that incorporates mass-ratio and asymptotes to zero for large kappa2T.
 */
double NRTunedTidesMergerFrequency(
            const double mtot_MSUN, /**< total mass of system (solar masses) */
            const double kappa2T,   /**< tidal coupling constant. Eq. 2 in arXiv:1706.02969 */
            const double q          /**< mass-ratio q >= 1 */
            )
{
  char errstr[200];

  if (q < 1.0) {
    snprintf(errstr, strlen(errstr),"XLAL Error - %s: q (%f) should be greater or equal to unity!\n",
       __func__, q);
    ERROR(PD_EDOM, errstr);
  }

  const double a_0 = 0.3586;
  const double n_1 = 3.35411203e-2;
  const double n_2 = 4.31460284e-5;
  const double d_1 = 7.54224145e-2;
  const double d_2 = 2.23626859e-4;

  const double kappa2T2 = kappa2T * kappa2T;

  const double num = 1.0 + n_1*kappa2T + n_2*kappa2T2;
  const double den = 1.0 + d_1*kappa2T + d_2*kappa2T2;
  const double Q_0 = a_0 / sqrt(q);

  /* dimensionless angular frequency of merger */
  const double Momega_merger = Q_0 * (num / den);

  /* convert from angular frequency to frequency (divide by 2*pi)
   * and then convert from
   * dimensionless frequency to Hz (divide by mtot * LAL_MTSUN_SI)
   */
  const double fHz_merger = Momega_merger / (TWOPI) / (mtot_MSUN * MTSUN_SI);

  return fHz_merger;
}


/** Function to call amplitude tidal series only; 
 * done for convenience to use for PhenomD_NRTidalv2 and 
 * SEOBNRv4_ROM_NRTidalv2
 */

int NRTunedTidesFDTidalAmplitudeFrequencySeries(
                   const RealVector *amp_tidal, /**< [out] tidal amplitude frequency series */
                   const RealVector *fHz, /**< list of input Gravitational wave Frequency [Hz or dimensionless] */
                   double m1, /**< Mass of companion 1 in solar masses */
                   double m2, /**< Mass of companion 2 in solar masses */
                   double lambda1, /**< (tidal deformability of mass 1) / m1^5 (dimensionless) */
                   double lambda2 /**< (tidal deformability of mass 2) / m2^5 (dimensionless) */
                   )
{
  double m1_SI = m1 * MSUN_SI;
  double m2_SI = m2 * MSUN_SI;
  double f_dim_to_Hz;

  int errcode = EnforcePrimaryMassIsm1(&m1_SI, &m2_SI, &lambda1, &lambda2);
  CHECK(PD_SUCCESS == errcode, errcode, "EnforcePrimaryMassIsm1 failed");

  const double mtot = m1 + m2;
  /** SEOBNRv4ROM_NRTidalv2 and IMRPhenomD_NRTidalv2 deal with dimensionless freqs and freq in Hz;
   *  If the value corresponding to the last index is above 1, we are safe to assume a frequency given in Hz, 
   *  otherwise a dimensionless frequency
   */

  if ((*fHz).data[(*fHz).length - 1] > 1.)
    f_dim_to_Hz = 1.;
  else 
    f_dim_to_Hz = mtot*MTSUN_SI;

  /**< tidal coupling constant.*/
  const double kappa2T = NRTunedTidesComputeKappa2T(m1_SI, m2_SI, lambda1, lambda2);

  for(size_t i = 0; i < (*fHz).length; i++)
    (*amp_tidal).data[i] = SimNRTunedTidesFDTidalAmplitude((*fHz).data[i]/f_dim_to_Hz, mtot, kappa2T);

  return PD_SUCCESS;
}

/**
 * Function to call the frequency domain tidal correction
 * over an array of input frequencies. This is
 * Equation (7) in arXiv:1706.02969 when NRTidal_version is NRTidal_V, 
 * or Equations (17)-(21) (for phasing) and Equation (24) (for amplitude) 
 * in arXiv:1905.06011 when NRTidal_version is NRTidalv2_V, 
 * or Equations (17)-(21) in arXiv:1905.06011 when NRTidal_version is NRTidalv2NoAmpCorr_V.
 * NoNRT_V specifies NO tidal phasing or amplitude is being added.
 * Note internally we use m1>=m2 - this is enforced in the code.
 * So any can be supplied
 *
 * The model for the tidal phase correction in NRTidal_V/NRTidalv2_V was calibrated
 * up to mass-ratio q=1.5 and kappa2T in [40, 5000].
 * The upper kappa2T limit is reached roughly for a
 * 1.4+1.4 BNS with lambda  = 2700 on both NSs.
 * In the high mass-ratio limit, the BNS merger frequency defined in
 * XLALSimNRTunedTidesMergerFrequency() asymptotes to zero. The waveform
 * amplitude should be tapered away starting at this frequency.
 * Therefore, no explicit limits are enforced.
 */

int NRTunedTidesFDTidalPhaseFrequencySeries(
               const RealVector *phi_tidal, /**< [out] tidal phase frequency series */
               const RealVector *amp_tidal, /**< [out] tidal amplitude frequency series */
               const RealVector *planck_taper, /**< [out] planck tapering to be applied on overall signal */
               const RealVector *fHz, /**< list of input Gravitational wave Frequency in Hz to evaluate */
               double m1_SI, /**< Mass of companion 1 (kg) */
               double m2_SI, /**< Mass of companion 2 (kg) */
               double lambda1, /**< (tidal deformability of mass 1) / m1^5 (dimensionless) */
               double lambda2, /**< (tidal deformability of mass 2) / m2^5 (dimensionless) */
               NRTidal_version_type NRTidal_version /** < one of NRTidal_V, NRTidalv2_V or NRTidalv2NoAmpCorr_V or NoNRT_V */
               )
{
  /* NOTE: internally m1 >= m2
   * This is enforced in the code below and we swap the lambda's
   * accordingly.
   */
  int errcode = EnforcePrimaryMassIsm1(&m1_SI, &m2_SI, &lambda1, &lambda2);
  CHECK(PD_SUCCESS == errcode, errcode, "EnforcePrimaryMassIsm1 failed");

  const double m1 = m1_SI / MSUN_SI;
  const double m2 = m2_SI / MSUN_SI;
  const double mtot = m1 + m2;
  const double q = m1 / m2;

  /* Xa and Xb are the masses normalised for a total mass = 1 */
  const double Xa = m1 / mtot;
  const double Xb = m2 / mtot;

  /**< tidal coupling constant.*/
  const double kappa2T = NRTunedTidesComputeKappa2T(m1_SI, m2_SI, lambda1, lambda2);

  /* Prepare tapering of amplitude beyond merger frequency */
  const double fHz_mrg = NRTunedTidesMergerFrequency(mtot, kappa2T, q);

  const double fHz_end_taper = 1.2*fHz_mrg;
  if (NRTidal_version == NRTidal_V) {
    for(size_t i = 0; i < (*fHz).length; i++){
      (*phi_tidal).data[i] = SimNRTunedTidesFDTidalPhase((*fHz).data[i], Xa, Xb, mtot, kappa2T);
      (*planck_taper).data[i] = 1.0 - PlanckTaper((*fHz).data[i], fHz_mrg, fHz_end_taper);
    }
  }
  else if (NRTidal_version == NRTidalv2_V) {
    for(size_t i = 0; i < (*fHz).length; i++) {
      (*phi_tidal).data[i] = SimNRTunedTidesFDTidalPhase_v2((*fHz).data[i], Xa, Xb, mtot, kappa2T);
      (*amp_tidal).data[i] = SimNRTunedTidesFDTidalAmplitude((*fHz).data[i], mtot, kappa2T);
      (*planck_taper).data[i] = 1.0 - PlanckTaper((*fHz).data[i], fHz_mrg, fHz_end_taper);
    }
  }
  else if (NRTidal_version == NRTidalv2NSBH_V) {
    for(size_t i = 0; i < (*fHz).length; i++) {
      (*phi_tidal).data[i] = SimNRTunedTidesFDTidalPhase_v2((*fHz).data[i], Xa, Xb, mtot, kappa2T);
      (*planck_taper).data[i] = 1.0;
    }
  }
  else if (NRTidal_version == NRTidalv2NoAmpCorr_V) {
    for(size_t i = 0; i < (*fHz).length; i++) {
      (*phi_tidal).data[i] = SimNRTunedTidesFDTidalPhase_v2((*fHz).data[i], Xa, Xb, mtot, kappa2T);
      (*planck_taper).data[i] = 1.0 - PlanckTaper((*fHz).data[i], fHz_mrg, fHz_end_taper);
    }
  }
  else if (NRTidal_version == NoNRT_V)
    ERROR( PD_EINVAL, "Trying to add NRTides to a BBH waveform!" );
  else
    ERROR( PD_EINVAL, "Unknown version of NRTidal being used! At present, NRTidal_V, NRTidalv2_V, NRTidalv2NSBH_V, NRTidalv2NoAmpCorr_V and NoNRT_V are the only known ones!" );

  return PD_SUCCESS;
}

/**
 * Function to add 3.5PN spin-squared and 3.5PN spin-cubed terms. 
 * The spin-squared terms occur with the spin-induced quadrupole moment terms
 * while the spin-cubed terms occur with both spin-induced quadrupole as well as 
 * octupole moments. The terms are computed in arXiv:1806.01772 and are 
 * explicitly written out in Eqn.27 of arXiv:1905.06011. The following terms 
 * are specifically meant for BNS systems, and are added to the NRTidalv2
 * extensions of the approximants IMRPhenomPv2, IMRPhenomD and SEOBNRv4_ROM. 
 */

void InspiralGetHOSpinTerms(
           double *SS_3p5PN, /**< 3.5PN spin-spin tail term containing spin-induced quadrupole moment */
           double *SSS_3p5PN, /**< 3.5 PN spin cubed term containing spin-induced octupole moment */
           double X_A, /**< Mass fraction m_1/M for first component of binary */
           double X_B, /**< Mass fraction m_2/M for second component of binary */
           double chi1, /**< Aligned component of spin vector of first component of binary */
           double chi2, /**< Aligned component of spin vector of second component of binary */
           double quadparam1, /**< Spin-induced quadrupole moment parameter for component 1 */
           double quadparam2 /**< Spin-induced quadrupole moment parameter for component 2 */
           )
{
  double chi1_sq = 0., chi2_sq = 0.;
  double X_Asq = 0., X_Bsq = 0.;
  double octparam1 = 0, octparam2 = 0.;

  X_Asq = X_A*X_A;
  X_Bsq = X_B*X_B;

  chi1_sq = chi1*chi1;
  chi2_sq = chi2*chi2;

  /* Remove -1 to account for BBH baseline*/
  octparam1 = UniversalRelationSpinInducedOctupoleVSSpinInducedQuadrupole(quadparam1)-1.;
  octparam2 = UniversalRelationSpinInducedOctupoleVSSpinInducedQuadrupole(quadparam2)-1.;

  *SS_3p5PN = - 400.*PI*(quadparam1-1.)*chi1_sq*X_Asq - 400.*PI*(quadparam2-1.)*chi2_sq*X_Bsq;
  *SSS_3p5PN = 10.*((X_Asq+308./3.*X_A)*chi1+(X_Bsq-89./3.*X_B)*chi2)*(quadparam1-1.)*X_Asq*chi1_sq
    + 10.*((X_Bsq+308./3.*X_B)*chi2+(X_Asq-89./3.*X_A)*chi1)*(quadparam2-1.)*X_Bsq*chi2_sq
    - 440.*octparam1*X_A*X_Asq*chi1_sq*chi1 - 440.*octparam2*X_B*X_Bsq*chi2_sq*chi2;
}


/**
 * Function to call the frequency domain tidal correction
 * over an array of input frequencies. This is
 * Equation (7) in arXiv:1706.02969 when NRTidal_version is NRTidal_V, 
 * or Equations (17)-(21) (for phasing) and Equation (24) (for amplitude) 
 * in arXiv:1905.06011 when NRTidal_version is NRTidalv2_V, 
 * or Equations (17)-(21) in arXiv:1905.06011 when NRTidal_version is NRTidalv2NoAmpCorr_V.
 * NoNRT_V specifies NO tidal phasing or amplitude is being added.
 * Note internally we use m1>=m2 - this is enforced in the code.
 * So any can be supplied
 *
 * The model for the tidal phase correction in NRTidal_V/NRTidalv2_V was calibrated
 * up to mass-ratio q=1.5 and kappa2T in [40, 5000].
 * The upper kappa2T limit is reached roughly for a
 * 1.4+1.4 BNS with lambda  = 2700 on both NSs.
 * In the high mass-ratio limit, the BNS merger frequency defined in
 * XLALSimNRTunedTidesMergerFrequency() asymptotes to zero. The waveform
 * amplitude should be tapered away starting at this frequency.
 * Therefore, no explicit limits are enforced.
 */
// int NRTunedTidesFDTidalPhaseFrequencySeries(
// int NRTunedTidesFDTidalPhaseFrequencySeries_h22FDAmpPhase(
//                const RealVector *phi_tidal, /**< [out] tidal phase frequency series */
//                const RealVector *amp_tidal, /**< [out] tidal amplitude frequency series */
//                const RealVector *planck_taper, /**< [out] planck tapering to be applied on overall signal */
//                const RealVector *fHz, /**< list of input Gravitational wave Frequency in Hz to evaluate */
//                double m1_SI, /**< Mass of companion 1 (kg) */
//                double m2_SI, /**< Mass of companion 2 (kg) */
//                double lambda1, /**< (tidal deformability of mass 1) / m1^5 (dimensionless) */
//                double lambda2, /**< (tidal deformability of mass 2) / m2^5 (dimensionless) */
//                NRTidal_version_type NRTidal_version /** < one of NRTidal_V, NRTidalv2_V or NRTidalv2NoAmpCorr_V or NoNRT_V */
//                )
// {
//   char errstr[200];
//   /* NOTE: internally m1 >= m2
//    * This is enforced in the code below and we swap the lambda's
//    * accordingly.
//    */
//   int errcode = EnforcePrimaryMassIsm1(&m1_SI, &m2_SI, &lambda1, &lambda2);
//   CHECK(PD_SUCCESS == errcode, errcode, "EnforcePrimaryMassIsm1 failed");

//   const double m1 = m1_SI / MSUN_SI;
//   const double m2 = m2_SI / MSUN_SI;
//   const double mtot = m1 + m2;
//   const double q = m1 / m2;

//   /* Xa and Xb are the masses normalised for a total mass = 1 */
//   const double Xa = m1 / mtot;
//   const double Xb = m2 / mtot;

//   /**< tidal coupling constant.*/
//   const double kappa2T = NRTunedTidesComputeKappa2T(m1_SI, m2_SI, lambda1, lambda2);

//   /* Prepare tapering of amplitude beyond merger frequency */
//   const double fHz_mrg = NRTunedTidesMergerFrequency(mtot, kappa2T, q);

//   const double fHz_end_taper = 1.2*fHz_mrg;
//   if (NRTidal_version == NRTidal_V) {
//     for(size_t i = 0; i < (*fHz).length; i++){
//       (*phi_tidal).data[i] = SimNRTunedTidesFDTidalPhase((*fHz).data[i], Xa, Xb, mtot, kappa2T);
//       (*planck_taper).data[i] = 1.0 - PlanckTaper((*fHz).data[i], fHz_mrg, fHz_end_taper);
//     }
//   }
//   else if (NRTidal_version == NRTidalv2_V) {
//     for(size_t i = 0; i < (*fHz).length; i++) {
//       (*phi_tidal).data[i] = SimNRTunedTidesFDTidalPhase_v2((*fHz).data[i], Xa, Xb, mtot, kappa2T);
//       (*amp_tidal).data[i] = SimNRTunedTidesFDTidalAmplitude((*fHz).data[i], mtot, kappa2T);
//       (*planck_taper).data[i] = 1.0 - PlanckTaper((*fHz).data[i], fHz_mrg, fHz_end_taper);
//     }
//   }
//   else if (NRTidal_version == NRTidalv2NSBH_V) {
//     for(size_t i = 0; i < (*fHz).length; i++) {
//       (*phi_tidal).data[i] = SimNRTunedTidesFDTidalPhase_v2((*fHz).data[i], Xa, Xb, mtot, kappa2T);
//       (*planck_taper).data[i] = 1.0;
//     }
//   }
//   else if (NRTidal_version == NRTidalv2NoAmpCorr_V) {
//     for(size_t i = 0; i < (*fHz).length; i++) {
//       (*phi_tidal).data[i] = SimNRTunedTidesFDTidalPhase_v2((*fHz).data[i], Xa, Xb, mtot, kappa2T);
//       (*planck_taper).data[i] = 1.0 - PlanckTaper((*fHz).data[i], fHz_mrg, fHz_end_taper);
//     }
//   }
//   else if (NRTidal_version == NoNRT_V)
//     ERROR( PD_EINVAL, "Trying to add NRTides to a BBH waveform!" );
//   else
//     ERROR( PD_EINVAL, "Unknown version of NRTidal being used! At present, NRTidal_V, NRTidalv2_V, NRTidalv2NSBH_V, NRTidalv2NoAmpCorr_V and NoNRT_V are the only known ones!" );

//   return PD_SUCCESS;
// }
