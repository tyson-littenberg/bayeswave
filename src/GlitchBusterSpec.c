/**************************************************************************
 
 Copyright (c) 2019 Neil Cornish
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 ************************************************************************/

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "GlitchBuster.h"
#include "GlitchBusterConstants.h"

//#ifndef _OPENMP
//#define omp ignore
//#endif

//OSX
// clang -Xpreprocessor -fopenmp -lomp -w -o Spec Spec.c GlitchBuster.c -lgsl  -lm

// CIT
// gcc -std=gnu99 -fopenmp -w -o Spec Spec.c GlitchBuster.c -lgsl -lm

// non parallel
// gcc -o Spec Spec.c -lm -lgsl

static void bwbpf(double *in, double *out, int fwrv, int M, int n, double s, double f1, double f2)
{
    /* Butterworth bandpass filter
     n = filter order 4,8,12,...
     s = sampling frequency
     f1 = upper half power frequency
     f2 = lower half power frequency  */
    
    if(n % 4){ printf("Order must be 4,8,12,16,...\n"); return;}
    
    int i, j;
    double a = cos(PI*(f1+f2)/s)/cos(PI*(f1-f2)/s);
    double a2 = a*a;
    double b = tan(PI*(f1-f2)/s);
    double b2 = b*b;
    double r;
    
    n = n/4;
    double *A = (double *)malloc(n*sizeof(double));
    double *d1 = (double *)malloc(n*sizeof(double));
    double *d2 = (double *)malloc(n*sizeof(double));
    double *d3 = (double *)malloc(n*sizeof(double));
    double *d4 = (double *)malloc(n*sizeof(double));
    double *w0 = (double *)malloc(n*sizeof(double));
    double *w1 = (double *)malloc(n*sizeof(double));
    double *w2 = (double *)malloc(n*sizeof(double));
    double *w3 = (double *)malloc(n*sizeof(double));
    double *w4 = (double *)malloc(n*sizeof(double));
    double x;
    
    for(i=0; i<n; ++i)
    {
        r = sin(PI*(2.0*(double)i+1.0)/(4.0*(double)n));
        s = b2 + 2.0*b*r + 1.0;
        A[i] = b2/s;
        d1[i] = 4.0*a*(1.0+b*r)/s;
        d2[i] = 2.0*(b2-2.0*a2-1.0)/s;
        d3[i] = 4.0*a*(1.0-b*r)/s;
        d4[i] = -(b2 - 2.0*b*r + 1.0)/s;
        w0[i] = 0.0;
        w1[i] = 0.0;
        w2[i] = 0.0;
        w3[i] = 0.0;
        w4[i] = 0.0;
    }
    
    for(j=0; j< M; ++j)
    {
        if(fwrv == 1) x = in[j];
        if(fwrv == -1) x = in[M-j-1];
        for(i=0; i<n; ++i)
        {
            w0[i] = d1[i]*w1[i] + d2[i]*w2[i]+ d3[i]*w3[i]+ d4[i]*w4[i] + x;
            x = A[i]*(w0[i] - 2.0*w2[i] + w4[i]);
            w4[i] = w3[i];
            w3[i] = w2[i];
            w2[i] = w1[i];
            w1[i] = w0[i];
        }
        if(fwrv == 1) out[j] = x;
        if(fwrv == -1) out[M-j-1] = x;
    }
    
    free(A);
    free(d1);
    free(d2);
    free(d3);
    free(d4);
    free(w0);
    free(w1);
    free(w2);
    free(w3);
    free(w4);
}

int main(int argc, char *argv[])
{
    
    
    int i, ND, N, m, dec;
    double *timeF, *dataF;
    double *times, *data;
    
    double x, y, dt, Tobs, ttrig, fny, fmin, fmax;
    double *SN=NULL;
    double *SM=NULL;
    double *PS=NULL;
    
    char command[1024];
    
    
    FILE *in;
    FILE *out;
    
    if(argc!=5)
    {
        printf("./Spec ifo Tobs trig_time fmax\n");
        return 1;
    }
    
    m = atoi(argv[1]);
    Tobs = atof(argv[2]);
    ttrig = atof(argv[3]);
    fmax = atof(argv[4]);
    
    
    sprintf(command, "frame_%d_%d_%d.dat", (int)(Tobs), (int)ttrig, m);
    
    in = fopen(command,"r");
    
    
    ND = -1;
    while(!feof(in))
    {
        fscanf(in,"%lf%lf", &x, &y);
        ND++;
    }
    rewind(in);
    // printf("Number of points = %d\n", ND);
    
    timeF = (double*)malloc(sizeof(double)* (ND));
    dataF = (double*)malloc(sizeof(double)* (ND));
    
    for (i = 0; i < ND; ++i)
    {
        fscanf(in,"%lf%lf", &timeF[i], &dataF[i]);
    }
    fclose(in);
    
    
    dt = timeF[1]-timeF[0];
    Tobs = (double)(ND)*dt;  // duration
    fny = 1.0/(2.0*dt);  // Nyquist
    
    // printf("Nyquist %f  fmax %f\n", fny, fmax);
    
    // if fmax < fny we can downsample the data by decimation
    // first we bandpass then decimate
    
    dec = (int)(fny/fmax);
    
    
    // printf("Down sample = %d\n", dec);
    
    fmin = 8.0;
    
    // apply 8th order zero phase bandpass filter
    bwbpf(dataF, dataF, 1, ND, 8, 1.0/dt, fmax, fmin);
    bwbpf(dataF, dataF, -1, ND, 8, 1.0/dt, fmax, fmin);
    
    N = ND/dec;
    
    times = (double*)malloc(sizeof(double)* (N));
    data = (double*)malloc(sizeof(double)* (N));
    
    // decimate
    for (i = 0; i < N; ++i)
    {
        times[i] = timeF[i*dec];
        data[i] = dataF[i*dec];
    }
    
    // reset sample rate and Nyquist
    dt = times[1]-times[0];
    fny = 1.0/(2.0*dt);
    
    
    sprintf(command, "framed_%d_%d_%d.dat", (int)(Tobs), (int)ttrig, m);
    
    out = fopen(command,"w");
    for (int i = 0; i < N; ++i) fprintf(out,"%.16e %.16e\n", times[i], data[i]);
    fclose(out);
    
    int Ns = (int)(Tobs*fmax);
    
    SM = (double*)malloc(sizeof(double)*(Ns));
    SN = (double*)malloc(sizeof(double)*(Ns));
    PS = (double*)malloc(sizeof(double)*(Ns));

    GlitchBusterSpecWrapper(data, N, Tobs, fmax, dt, SN, SM, PS);
    
    sprintf(command, "PSD_%d_%d_%d.dat", (int)(Tobs), (int)ttrig, m);
    out = fopen(command,"w");
    for (int i = 0; i < Ns; ++i)
    {
        fprintf(out,"%.15e %.15e %.15e\n", (double)(i)/Tobs, SN[i], SM[i]);
    }
    fclose(out);

    
    sprintf(command, "dataF_%d_%d_%d.dat", (int)(Tobs), (int)ttrig, m);
    out = fopen(command,"w");
    for(int i = 1; i < N/2; i++) fprintf(out,"%.16e %.16e\n", data[i], data[N-i]);
    fclose(out);

    
    return 0;
    
}


