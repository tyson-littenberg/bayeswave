#!/bin/bash -xe
# ----------------------------------------- #
#                 BAYESWAVE                 #
# ----------------------------------------- #

# source this file to access bayeswave
export BAYESWAVE_PREFIX=INSTALL_DIR

# Identify python version
pymajor=$(python -c 'import sys; print(sys.version_info[0])')
pyminor=$(python -c 'import sys; print(sys.version_info[1])')

# BayesWave
# TODO we should deprecate this and enforce that everyone installing from source
#   installs in an actual environment when they do so, then we will never need to update paths like this
export PATH=$BAYESWAVE_UTILS_PREFIX/bin:${BAYESWAVE_UTILS_PREFIX}:${BAYESWAVE_PREFIX}/bin:${PATH}
export LD_LIBRARY_PATH=${BAYESWAVE_PREFIX}/lib:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=${BAYESWAVE_PREFIX}/lib64:${LD_LIBRARY_PATH}
export DYLD_LIBRARY_PATH=${BAYESWAVE_PREFIX}/lib:${DYLD_LIBRARY_PATH}
export DYLD_LIBRARY_PATH=${BAYESWAVE_PREFIX}/lib64:${DYLD_LIBRARY_PATH}
export PYTHONPATH=${BAYESWAVE_PREFIX}/lib/python${pymajor}.${pyminor}/site-packages:${PYTHONPATH}

