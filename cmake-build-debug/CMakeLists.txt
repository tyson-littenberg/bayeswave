#
# CMake packaging for BayesWave
# Copyright 2019  James Alexander Clark <james.clark@ligo.org>
# Based on CMake packaging for libframe & Frv by Duncan MacLeod <duncan.macleod@ligo.org>
#

# -- package info -----------

cmake_minimum_required(VERSION 3.12.0 FATAL_ERROR)
project(
    bayeswave
    LANGUAGES C
    VERSION 1.0.6
    DESCRIPTION "LIGO/VIRGO burst analysis algorithm"
    HOMEPAGE_URL "https://git.ligo.org/lscsoft/bayeswave"
)

include(GNUInstallDirs)
include(CheckFunctionExists)
find_package(PkgConfig)
find_package(Git)
find_package(OpenMP)

# -- build components -------

# C library | enable/disable with -DENABLE_C={yes,no} (default yes)
add_subdirectory(src)
#add_subdirectory(doc)

# -- packaging components ---

set(SPEC_IN "${CMAKE_CURRENT_SOURCE_DIR}/bayeswave.spec.in")
set(SPEC    "${CMAKE_CURRENT_SOURCE_DIR}/bayeswave.spec")
configure_file(${SPEC_IN} ${SPEC} @ONLY)

# -- build tarball ----------
#
# to build a source tarball:
#
# mkdir dist
# pushd dist
# cmake ..
# cmake --build . --target package_source
#

set(CPACK_PACKAGE_MAJOR ${${PROJECT_NAME}_MAJOR_VERSION})
set(CPACK_PACKAGE_MINOR ${${PROJECT_NAME}_MINOR_VERSION})
set(CPACK_PACKAGE_PATCH ${${PROJECT_NAME}_PATCH_VERSION})
set(CPACK_PACKAGE_VERSION "${CPACK_PACKAGE_MAJOR}.${CPACK_PACKAGE_MINOR}.${CPACK_PACKAGE_PATCH}")

set(CPACK_SOURCE_GENERATOR TXZ)
set(CPACK_SOURCE_PACKAGE_FILE_NAME ${PROJECT_NAME}-${${PROJECT_NAME}_VERSION})
set(CPACK_SOURCE_IGNORE_FILES
    "/.*~$/"
    ".*~$"
    "\\\\.svn/"
    "\\\\.git"
    "build/"
    "CMakeFiles/"
    "CMakeCache.txt"
    "_CPack_Packages/"
    "\\\\.cmake"
    "Makefile"
    "\\\\.deps/"
    "autom4te.cache/"
    "\\\\.tar\\\\.xz"
)
include(CPack)
